## 炫彩IDE

包含:炫彩UI设计器, 炫彩中文编程语言

此处提供代码示例,教程,说明,以帮助用户学习使用

http://www.xcgui.com/

QQ群: 2283812(3号群), 验证码:XCGUI


![](README.assets/炫彩-IDE.jpg)



<center><h2>炫彩编程手册</h2></center>
<p align="right">更新日期:2021-02-06



####  简介

- 界面库设计器与编程语言结合,  集成化开发, 提高开发效率, 减少工具切换, 减少代码输入, 降低重复工作

- 炫彩编程python风格精简语法, 支持全中文开发,轻量级开发环境, c++运行效率

- 帮助想学编程,又不懂英文的用户

- 帮助业余编程人员学习编程

- 帮助个人开发者简单快速开发软件

- 帮助专业程序员更好的享受编程

- 从入门 ->到进阶 -> 到大神, 适合不同层次专业或业余编程人员, 个人或团队开发

- 炫彩IDE 集界面设计 编码  编译 调试 发布,

- 未来将支持跨平台,跨语言

- 编程开发学习一个工具就够了,不必浪费精力学习其他编程语言和工具

>UI, 编码, 集成, 可视化, 简单, 快速, 运行效率,中文,英文, 从入门到大神之路
----------

- *.xh  特殊头文件  包含外部C/C++头文件,  声明外部函数,数据类型, 宏定义

- *.xc  源文件    源代码文件

- 代码对齐TAB: 强制使用TAB对齐代码块, 不可用空格对齐

- pass: 空行占用

- 支持与C++代码混合, 支持调用外部DLL

- 支持 中英双文变量函数名称

##### 特性

- 炫彩编程IDE,集成化, UI可视化, 减少开发中工具的切换, 增加集成度, 智能化, 减少重复工作

- 内置炫彩界面库支持

- 支持中文编程, 支持英文编程,  满足不同的人群 

- 新的语法和封装,  相比C++入门更简单, 去除头文件及前置声明概念, 精简语法, 减少代码输入量

- 与C++相同的性能, 本身底层就是C++编译

- 可扩展 支持与C++代码混合, 支持调用DLL, C++的库都可以拿来用

- 支持UNICODE, x64

- python 代码风格,  代码更加精简整洁

- 未来支持跨平台, 支持转换为其他编程语言程序

-  代码编辑器双模式; 支持代码模式(纯文本)与代码表格模式(类似易语言代码表格)切换

#### 环境安装

##### 下载IDE
炫彩QQ群共享下载, 群号:1653999

##### 环境设置

默认附带VC14开发库, 如果需要其他版本VC请自己修改配置,方法如下:

将 "data\VC\VC14.0-Debug-DLL\\" 目录下DLL文件复制到: "C:\Windows\\"
或者""C:\Windows\System32\\" 
或者 添加到环境变量PATH里
(如果已安装了VC14 可忽略)

模块文件夹路径: data\VC\XC\

>发布Release模式下编译的exe, 默认是静态编译, 不依赖VC环境DLL

>默认带的运行环境vs2015 - VC14.0, 如果使用其他VC版本环境需修改配置文件 "data\config\config.config"

#### 数据类型

- 整型(int)  4字节

- 无符号整型(unsigned int, UINT) 4字节

- 布尔型(BOOL, int) 4字节

- 布尔型c(bool) 1字节

  

- 短整型(short, SHORT)  2字节

- 无符号短整型(unsigned short, USHORT) 2字节

- 字符型(wchar_t)  2字节

  

- 字符型A(char) 1字节

- 字节型(unsigned char, BYTE)  1字节

  

- 浮点型(double) 8字节

- 短浮点型(float) 4字节

  

- 文本(字符串操作对象,双字节编码)

- 文本A(字符串操作对象,多字节编码)

  

- 指针(\*)    4字节    例如: wchar_t\*,   int\*

- 变整型(32位程序4字节, 64位程序8字节)

- 无符号变整型(32位程序4字节, 64位程序8字节)

- 无类型(void)

  

```c
整型    type_int= 1000
短整型  type_short=100
字节型  type_byte=10
浮点型  type_float=3.14	
整型    type_array_int[10]={0}
文本    type_text = "123-ABC"
 
调试输出("测试_数据类型 整型: ", type_int, "  大小:",sizeof(type_int))
调试输出("测试_数据类型 短整型: ", type_short, "  大小:",sizeof(type_short))
调试输出("测试_数据类型 字节型: ", type_byte, "  大小:",sizeof(type_byte))
调试输出("测试_数据类型 浮点型: ", type_float, "  大小:",sizeof(type_float))
调试输出("测试_数据类型 数组: ", type_array_int, "  大小:",sizeof(type_array_int))	
调试输出("测试_数据类型 文本: ", type_text, "  大小:",sizeof(type_text))
```

#### 运算符

| + | 加 |
|----|----|
|-  |  减|
|* |  乘|
|/ | 除|
|= | 等于|
|%|  求余|
|++ |  自增1|
|\-\- | 1 自减1|
|+= | 加等于|
|-= | 减等于|
| ==  | 等于 |
|!=   | 不等于|
|\<   |  小于|
|\>   |  大于|
|\<=     |        小于等于|
|\>= | 大于等于|
|&& |    逻辑与|
| \|\| |  逻辑或|
|! | 取反|
|& | 位与|
|\|   |   位或|
|^   |   异或|
|\<\< |  位左移|
| \>\> |  位右移|

#### 注释

单行注释
 ```c
整型   a  //单行注释
 ```

 多行注释(开发中...)
 ```c
整型  a
/* 多行注释内容
整型  b
整型  c
*/
整型  d
整型  e
 ```

#### 快捷键

F5: 编译和调试
F7: 编译
F9:下断点
F10: 单步
F11: 步入

TAB: 缩进
TAB + SHIFT: 减少缩进
"/": 注释/取消注释
Ctrl + G: 跳转到指定行
Ctrl + F: 搜索和替换

#### 区块操作

选择多行内容 鼠标右键菜单:
->注释
->增加缩进
->取消缩进

#### [+]流程控制

##### 如果...否则 if...else

```c
//如果  判断条件
//    条件成立
//否则
//    条件不成立

整型  a = 1
整型  b = 2
如果  a == 1
    调试输出("a==1")
否则 如果 a==2
    调试输出("a==2")
否则
    调试输出("a==?")

如果  (a==1) && (b==2) //当 a=1 并且 b=2 时 条件成立
	pass
```

##### 循环 while

```c
//循环 条件
//   执行代码

整型 i=0
循环 i<5
	调试输出(i)
	i++
```

##### 变量循环 for

```c
变量循环  整型 i=0;  i<3;  i++
	调试输出(i)
```

##### 计次循环 for 
```c
计次循环 整型 i=0; 3  //变量i: 为循环索引; 数字3: 为循环次数; 当i=0时循环3次(0,1,2), 当i=1时循环2次(1,2)
	调试输出(i)  //输出索引 i 的值
```

##### 列表循环 for 
//普通数组循环
```c
整型  数组A[3]={0}
列表循环 整型 i;  数组A //将循环3次  0-3;  变量i: 接收当前索引值, 数组A: 数组变量
    调试输出(数组A[i])
```

//动态数组循环
```c
动态数组<整型>  数组A  // 尖括号<>内, 可以是任意数据类型, 比如: 整型, 文本, 指针, 对象; 理解为C++的模板
数组A.添加项(1)
数组A.添加项(2)
数组A.添加项(3)
列表循环  整型 i; 数组A
	调试输出(数组A[i])
```

//字典循环
```c
字典<整型, 整型>  字典A
字典A[1]=10
字典A[2]=20
字典A[3]=30
列表循环  迭代器 迭代器项; 字典A   //迭代器项: 接收当前返回的结果
	调试输出( 迭代器项.键, " | ", 迭代器项.值) //打印当前项的键和值
```

##### 分支判断 switch

分支判断:  需要手动添加"跳出"命令

分支判断跳出: 自动跳出分支

```c
def 整型 测试_分支判断()
	调试输出("测试_分支判断")
	整型   a=1
	分支判断  a
		判断  1
			调试输出(1)
			跳出
		判断 2
			调试输出(2)
			跳出
		其他
			调试输出(3)
	
	a = 3
	分支判断跳出 a
		判断  1
			调试输出(1)
		判断 2
			调试输出(2)
		其他
			调试输出(3)
	
	返回 0
```

##### 跳出

```c
循环  TRUE
	跳出  //跳出循环
```

##### 继续

```c
循环  TRUE
    继续   //跳过后面循环内容, 直接跳转到下一次循环
    调试输出("123")
```

#### [+]程序入口

##### 控制台程序 示例

```c
def 整型  入口函数()
	调试输出("你好! 炫彩编程")
	返回 0
```
##### 窗口程序 示例

```c
#库文件  "xcgui.lib"
#头文件  "xcgui.h"
#宏定义  SW_SHOW  5

HWINDOW hWindow=0
def  整型 回调  My_EventBtnClick(整型* pbHandled)    //按钮点击事件
	调试输出("按钮-关闭窗口")
	*pbHandled=1
	返回 0

def 整型 入口函数_窗口() 
	XInitXCGUI()       //初始化界面库
	hWindow=XWnd_Create(0,0,500,300, "xcgui-window") //创建窗口
	如果 hWindow
		HELE hButton=XBtn_Create(20,60,80,25,"关 闭",hWindow) //创建按钮
		XBtn_SetTypeEx(hButton, button_type_close)
		
		XEle_RegEventC(hButton,XE_BNCLICK,My_EventBtnClick) //注册事件
		XWnd_AdjustLayout(hWindow) //调整布局
		XWnd_ShowWindow(hWindow,SW_SHOW) //显示窗口
		XRunXCGUI()
	XExitXCGUI()
	返回 0

```

#### [+]数据容器

##### 高级数组
高级动态数组
待补充...

##### 高级字典
待补充...

##### 数组
```c
整型   整型数组[10]={0}  //定义10个成员的整型数组, 初始每个成员值为0
浮点型 浮点数组[10]={0}
```
##### C++容器字典 std::map
对应模块文件 map_wrap.h; 用户可修改文件对其扩展
字典<键,值>    变量名
模块接口见IDE中模块

```c
 字典<键,值>  变量名
```

```c
字典<文本, 整型>  字典A
字典A["aaa"] = 1
字典A["bbb"] = 2
字典A["ccc"] = 3
 
整型  大小 = 字典A.大小()  //取字典成员数量
整型  a = 字典A["aaa"]    //取键"aaa" 对应的值
 
迭代器  迭代器_查找_a = 字典A.查找("aaa") //或者: 字典<文本, 整型>::迭代器 迭代器_查找_a = 字典A.查找("aaa")
如果  迭代器_查找_a != 字典A.空值()
    调试输出("找到")
否则
    调试输出("未找到")

列表循环  迭代器 迭代器项; 字典A
	调试输出( 迭代器项.键, " | ", 迭代器项.值)
```

##### C++动态数组 std::vector
对应模块文件vector_wrap.h 用户可修改文件对其扩展
动态数组<类型>  变量名
模块接口见IDE中模块
```c
动态数组<整型>  数组A;
数组A.添加项(1)
数组A.添加项(2)
数组A.添加项(3)
 
整型  数组大小 = 数组A.大小()
 
列表循环 整型 i; 数组A
    调试输出(数组[i])
 
列表循环  整型  i; 数组A
    调试输出(数组A[i])
```

#### 函数 function
如果不检查参数, 声明函数时参数"..."

"def" 也可以使用中文 "函数"

```c
def 返回值  函数名(...) //IDE不检查参数; 例如当参数类型为任意型时或参数数量不固定时
```

```c
def 整型  相加(整数 a, 整数 b)
    返回  a+b
 
def 整型 回调 按钮点击事件(整型* pbHandled)
    调试输出("按钮被点击")
    返回 0
```

#### [+]类 class
构造函数 和 析构函数没有返回值

```c
类  测试类
    整型  _a //成员变量
 
    def构造()  //构造函数
       _a = 10
 
    def析构()  //析构函数
       pass  //pass占位行, 没有实际意义
 
    def 整型  Add(整型 a, 整型 b)  //成员函数
        返回  a+b+_a
 
测试类  测试类_
整型  c = 测试类_.Add(1,2)
调试输出(c)
```
```c
类  大小
    整型  宽度
    整型  高度
 
大小   大小_
调试输出("宽度=", 大小_.宽度)
调试输出("高度=", 大小_.高度)
```

#####  类的继承
```c
类  CA
	def无返回  Add(整型 a, 整型 b)
		调试输出("CA::Add()")

类  CB 继承 CA
	def 无返回  Add()
		调试输出("CB::Add()")

def 整型 入口函数_窗口()
	CB   cb
	cb.Add()
	
	CA*  ca = &cb //取地址
	ca->Add(1,3)
	
	返回 0
```

#####  类虚函数
虚函数: 子类有的调用子类, 子类没有的调用父类的
子类重写虚函数可以覆盖父类的函数功能
析构函数也可以为虚函数, 好处是用基类的指针释放时自动调用子类析构函数,否则子类析构函数不会被触发

```c
类  CA
	def 虚函数 无返回  Add(整型 a, 整型 b)
		调试输出("CA::Add()")

类  CB 继承 CA
	def 虚函数 无返回  Add()
		CA::Add()  //可强制调用父类中的CA::Add()函数
		调试输出("CB::Add()")
		
def 整型 入口函数_窗口()
	CB  cb
	cb.Add()   //调用CB类中的Add()函数
	
	CA ca= cb //转换对象类型为CA,  因为CA是CB的基类,所以可以转换
	ca.Add()  //调用CB类中的Add() 函数
	
	//因为CB类重写了CA类中的Add()函数, 所以覆盖了CA::Add()
	返回 0
```

#### 结构体 struct

```c
结构体   SIZE_1
    整型  cx
    整型  cy
    
def 整型 入口函数_窗口()
    SIZE_1  size
    size.cx=10
    size.cy=20
```



#### new delete 操作

```c
RECT* rc=new RECT  //new 对象
rc->left=10        //赋值
调试输出(rc->left)  //打印输出
delete rc          //删除对象
```

#### 取变量地址
取普通变量地址
```c
整型  a = 10   //定义整型变量
整型* pA = &a  //取变量地址
调试输出(pA)    //打印地址
调试输出(*PA)   //打印值
```

取类变量地址
```c
RECT   rc          //定义对象
RECT*  pRc = &rc   //取对象地址
调试输出(&rc)       //打印对象地址
```

#### 定义宏(内部定义宏 与 声明外部宏)
```c
#宏定义  SW_SHOW  5 
#外部宏  SW_SHOW  //声明在外部文件中已经存在的宏
```
#### [+]技巧
##### 调试输出(UNICODE编码)
UNICODE编码, 双字节 wchar_t*
```c
调试输出("打印一行信息到输出窗口") 

整型  a
文本  text="你好! 炫彩编程"
调试输出("a=",a, ",  text=", text) //打印变量值
```
##### 调试输出(多字节编码)
在字符串前面加字幕"A"代表多字节编码 char*
```c
调试输出(A"打印一行信息到输出窗口") 

整型  a
文本A  text=A"你好! 炫彩编程"
调试输出(A"a=",a, A",  text=", text) //打印变量值
```
##### 占位空行 pass
```c
def 无返回  函数名()
	pass
```
##### 一行写多条语句

分号";"代表后面语句不换行
冒号":"代表后面语句是子集块

```c
整型  a = 1;  整型  b = 2  //一行写多条语句用 ; 分开
整型  c = a + b
```
##### 子集块不换行写法
```c
整型  a=10
如果  10==a : 调试输出("a==10"); 调试输出("条件成立") //将子集块写在同行
 
如果  10!=a : 调试输出("a!=10");
否则 : 调试输出("a==10")
```

##### 多段字符串拼接
```c
文本  text = "123""456"
"789"
//结果为:text = "123456789"
```

#### [+]扩展 
##### 包含外部头文件

```c
#头文件  <helper.h>  //系统头文件, 不解析文件内容
#头文件  "xcgui.h"   //普通头文件, 解析文件内容
#库文件  "xcgui.lib"  //静态库或动态库lib文件
```
> 可使用隐藏命令,隐藏不需要解析的代码,提高解析速度,排除无法解析的内容; 见模块封装

##### 外部函数及类型声明

如果某个函数已存在外部代码或库中, 只需要在在*.xh文件中声明类型

例如:

声明外部函数, 参数中 "..."三个点代表不检查参数,省略掉
```c
int sizeof(...)  //声明外部函数
void Sleep(int dwMilliseconds)
```

声明外部类
```c
类   RECT
类   CSize
```

声明外部基础类型, 或未知类型
```c
#外部类型  HDC    //声明外部定义的类型
#外部类型  HANDLE //声明外部定义的类型
```
##### 调用外部库 DLL
>例如: 调用炫彩界面库DLL
>DLL-头文件: xcgui.h
>DLL-lib文件:xcgui.lib

![](https://img-blog.csdnimg.cn/20190817180429632.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21lbmdmZWlnbw==,size_16,color_FFFFFF,t_70)
##### 与C++代码混合
与C++代码混合使用方法

test.h  头文件
```c
int  add(); //声明函数
```

test.cpp 源文件
```c
#include "common__.h" //包含预编译
 
int  add()
{
	OutputDebugString(L"123\n");
	return 99;
}
```

或者在*.xh文件中声明外部函数

在*.xc源文件中 包含c++ 头文件和源文件, 调用c++代码中的add()函数

![](https://img-blog.csdnimg.cn/20191002155815560.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21lbmdmZWlnbw==,size_16,color_FFFFFF,t_70)



##### 嵌入C++代码

如果与函数对齐, 作用域为函数内

```c
#嵌入代码
此处嵌入C++代码
#嵌入代码_结束
```

![](README.assets/111-1612342837859.jpg)

#### [+]模块

模块默认位置: IDE文件夹\data\VC\XC\

模块文件:module_*.h,    module\_开头的(\*.h)文件为模块文件

IDE启动时, 自动搜索此目录下的模块文件

模块中只列出类的公开成员(public)

可使用隐藏命令, 隐藏不需要解析的代码, 排除无法解决的代码, 提高解析速度

可使用分组命令, 对全局函数进行分组, 便于浏览管理



##### 模块选择对话框

项目视图->右键菜单->选择模块

选择当前项目中使用的模块

![](README.assets/222.png)

##### 模块浏览器

查看模块中的接口及说明

![](README.assets/444.png)

##### 调用模块

项目视图->右键菜单->选择模块,  勾选模块 "字符串操作" module_test.h
```c
def 整型 测试模块()
	字符串类  字符串="abc"
	字符串.增加字符串("123")
	整型 位置 = 字符串.查找("123")
	返回 字符串.获取大小()
```

#### [+]模块封装

>参考示例: IDE中 module_base.h  模块包装; 位置: IDE文件夹\data\VC\XC\module_base.h

###### 别名

声明(类, 变量, 函数, 函数参数名) 别名, 以便在IDE中使用中文名称调用

```c
//@rename  别名(别名, 别名, ...)
```
###### 声明

对已存在的(类, 函数)别名, 使用多行注释包裹声明代码

```c
/*@声明
代码...
*/
```

###### 依赖

例如 "炫彩界面库-类" 依赖  "炫彩界面库C(module_xcgui.h)"

```c
//@模块名称  炫彩界面库-类
//@依赖   module_xcgui.h
```



###### 包含CPP文件

在头文件中声明包含cpp文件,(加入编译文件列表)
如果直接在(*.h)文件中包含cpp文件, 当多个文件中包含该(*.h)文件时会造成重复的实现, 例如:函数已在(*.obj)模块中已存在,造成重复
所以这里采用特殊方法声明要包含的cpp文件, IDE编译代码时, 自动连接cpp文件,避免重复的实现

```c
/*@声明
#src "pugixml.cpp"  //例如包含cpp文件 pugixml.cpp
*/
```

###### 包含lib文件

```c
/*@声明
#lib "xcgui.lib"   //包含lib文件
*/
```

###### 模块文件说明信息

在模块文件开始位置加入模块详情, 例如:

```c
//@模块名称  炫彩界面库-c接口  
//@版本  1.0  
//@日期  2020-10-23
//@作者  XCGUI  
//@模块备注  炫彩界面库-c接口  
// 备注换行
```

###### 函数参数说明

```c
//@备注 两个数相加
//@参数 a  输入一个整数
//@参数 b  输入一个整数
//@返回 返回a+b结果
//@别名 相加(整型, 整型)
int Add(int a, int b)
{
    return a+b;
}
```

###### 隐藏代码

```c
//@隐藏{
此处的代码不会被解析, 跳过
//@隐藏}
```



###### 分组

```c
//@分组{   组名称
组内代码, 例如: 类, 全局变量, 全局函数, 结构体, 枚举, 宏
//@分组}
```



######  #智能指针

> 此接口宏是为了自动模拟智能指针操作,减少重复工作, 手动模拟请看后面章节

模拟C++ 智能指针接口, 默认模拟的接口不检查参数(为了适应更多接口变化)

```c
//假如模拟下面的智能指针为例
#智能指针  CefRefPtr<CefBrowser>  CefBrowserPtr

//CefRefPtr  智能指针类
//CefBrowser 包装的对象
//CefBrowserPtr  别名类

//那么T为CefBrowser*
//默认自动模拟以下接口, 并且继承CefRefPtr中的接口,
//如果没有解析CefRefPtr, 可以模拟此类及其接口
T* get()   //CefBrowser* get()
addRef()
addref()
AddRef()
Release()
release()
T* operator->()
//CefRefPtr 中的接口

```

用法示例

```c
#智能指针  CefRefPtr<CefBrowser>  CefBrowserPtr  //模拟智能指针接口, 用法与C++中一样

CefBrowserPtr   m_cefBrowser  //定义智能指针变量
m_cefBrowser->GetHost()       //调用CefBrowser接口GetHost()
CefBrowser* pCefBrowser = m_cefBrowser.get() //调用智能指针自身接口

```



###### #智能指针_仅声明

仅声明智能指针接口, 例如C++源码中已经定义了智能指针类型, 仅声明接口即可

```c
.h文件
//假如已经定义智能指针类型
typedef CHPSocketPtr<ITcpClient, ITcpClientListener, TcpClient_Creator>    CTcpClientPtr;
```



```c
.xc文件

#智能指针_仅声明    CHPSocketPtr<IHttpClient>   CHttpClientPtr  //仅声明接口, 避免造成重复
```



###### 模拟-模板继承

解析C++文件时, 不支持模板解析, 所以需要模拟接口

```c
//test.h
//C++ 原文件
//假如 T1 是 class CA
//假如 T2 是 class CB
template<class T, class T2> 
class CMyClass : public T, T2 //CMyClass 继承类 T1 T2, 拥有T1和T2 的全部接口
{
};
typedef CMyClass<CA,CB>	 CMyClassD;

//---------模拟封装接口------------
/*@声明
//@备注  自定义双接口类
//@别名  测试类
class CMyClassD : public CA, CB
{
};
*/
```

```c
//test.xc
//测试模块
CMyClassD  myClass //继承 CA, CB成员     CMyClass<CA,CB>  myClass
myClass.CA成员  //可调用CA成员
myClass.CB成员  //可调用CB成员

```



###### 模拟-模板智能指针

假如是智能指针, 需要模拟智能指针接口 和 重载操作符继承接口

```c
//text.h
//C++ 原文件
//假如 T 是 class CA
template<class T>
class CPtr
{
public:
    T*  operator->();
    void  operator=(T* pObj){ m_pObj = pObj;}
    T*  Attach(CMyClass *pClass);
    T*  Detach();
private:
     *  m_pObj;
}
typedef CPtr<CA>	CaPtr;

class CA
{
public:
    int Add(int a, int b){ return a+b;}
};

//---------模拟封装接口------------
/*@声明
class CaPtr
{
public:
	CA* operator->();  //模拟此接口 可以调用CA成员
	CA* Attach(CA* pObj);
	CA* Detach();
private:
    T*  m_pObj;
};
*/
```



```c
//test.xc
//测试模块
CaPtr   ptr = new  CA  //  CPtr<CA>  ptr = new CA
ptr->Add(1,2) //调用 CA成员函数
ptr.Detach()  //调用 智能指针函数
```



#### 模块封装示例

```c
//假如 CStringBase 类已存在,原型如下
class CStringBase
{
public:
	int GetSize();
	const wchar_t* GetString();
	void AddString(const wchar_t* pString)
};
```

模块文件名: module_test.h
将此文件放入目录   IDE文件夹\data\VC\XC\module_test.h

```c
//module_test.h
//定义CMyString类 继承CStringBase类, 并且对基类CStringBase中的函数别名
//CMyString 类中实现查找功能函数 Find(), 添加成员变量 m_status

#ifndef MY_TEST_WRAP_H
#define MY_TEST_WRAP_H

//@模块名称  字符串操作
//@版本  1.0  
//@日期  2020-10-23
//@作者  XCGUI  
//@模块备注  字符串操作示例模块,
//此示例仅供参考

/*@声明
#src "module_text.cpp"  //包含cpp文件
#lib "xcgui.lib"   //如果用到礼拜文件
*/

//@别名 字符串类
class CMyString : public CStringBase
{
public:
/*@声明   //此处是对已存在的函数 声明
	//@别名 获取大小()
	int GetSize()

	//@别名 获取字符串指针()
	const wchar_t* GetString();
	
	//@备注  增加字符串到末尾
	//@参数  pString  要添加的字符串
	//@返回  无返回值
	//@别名  增加字符串(字符串)
	void AddString(const wchar_t* pString)
*/
	//@别名 查找(字符串)
	int Find(const wchar_t* pString)
	{
		int ret = -1;
		//查找代码...
		return ret;
	}
	//@别名 状态
	int  m_status; //状态
};
#endif
```

#### [+]炫彩编程 - IDE -界面库

左侧的函数列表, 当保存代码即时更新

涉及到以下窗格:
函数列表
断点列表
调用堆栈
局部变量
监视
调试输出
搜索和替换
查找引用
模块视图

![](README.assets/IDE.jpg)



![](README.assets/IDE2.jpg)



##### 新建项目(炫彩编程)

![](https://img-blog.csdnimg.cn/20190930232952775.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21lbmdmZWlnbw==,size_16,color_FFFFFF,t_70)



##### UI布局文件生成代码类

![](https://img-blog.csdnimg.cn/20190930232124732.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21lbmdmZWlnbw==,size_16,color_FFFFFF,t_70)
##### UI生成事件代码

![](https://img-blog.csdnimg.cn/20190930232423138.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21lbmdmZWlnbw==,size_16,color_FFFFFF,t_70)
![](https://img-blog.csdnimg.cn/20190930232318681.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21lbmdmZWlnbw==,size_16,color_FFFFFF,t_70)

##### 项目属性-Debug/Release
设置Debug\Release编译选项 配置属性

![](README.assets/111.jpg)



选择类型 可以设置编译exe,动态库DLL, 静态库lib

新建按钮, 可从现有配置中复制新的配置修改

新建配置可保存为全局配置,或当前项目配置



##### 导出动态库DLL

项目属性 -> 炫彩编程 -> 类型: 动态库(.dll)

在要导出的函数前面加上 "导出"

```c
//导出函数
def 导出 整型 add(整型 a, 整型 b)
	返回 a+b
```



##### 导出静态库LIB

项目属性 -> 炫彩编程 -> 类型: 静态库(.lib)



##### 事件助手

例如:  按钮.注册事件->事件助手 , 弹出[事件助手] 选择对应事件, 回车生成事件代码



##### 搜索替换

![](README.assets/555.jpg)

Ctrl + F 快捷键[搜索和替换]

结果输出在 [搜索和替换] 窗格中



##### 查找引用

在函数上鼠标 右键菜单->查找所有引用

结果输出在 [查找引用] 窗格内



##### 转到定义

在变量上按鼠标右键弹出菜单->转到定义,  跳转到指定文件行



##### 跳转到指定行

Ctrl + G 快捷键



##### 插入(函数 变量 类)

代码编辑器鼠标右键 菜单->插入->



##### 资源文件 resource.rc

>新建项目 勾选炫彩编程,  在项目文件夹下,将生成RC资源文件和程序图标
>用记事本工具打开编辑文件 resource.rc
>可添加 图标, 版权 等内容
>默认第一个图标为exe文件图标
>如果项目目录下没有资源文件,那么编译时忽略rc资源, 资源文件固定名称为 [resource.rc]

##### 用VS修改RC资源文件
如果不会编辑RC文件, 可以用VS修改
注意: vs中rc资源文件是包含 "resource.h" 头文件, 因为炫彩不需要,所以没有
自己把这一行代码注释取消,随便新建个 "resource.h"文件, 不然VS会提示找不到"resource.h"
![在这里插入图片描述](https://img-blog.csdnimg.cn/20191023002234903.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21lbmdmZWlnbw==,size_16,color_FFFFFF,t_70)



#### IDE调试

F5: 编译和调试

F7: 编译

F9:下断点

F10: 单步

F11: 步入