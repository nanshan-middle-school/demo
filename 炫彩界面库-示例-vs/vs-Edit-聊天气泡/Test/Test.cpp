// Test.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "Test.h"

//包含炫彩界面库文件
#include "../../../DLL/xcgui.h"
#pragma comment(lib,"../../../DLL/XCGUI.lib")

#include <vector>
using namespace std;

class CEditRecv
{
public:
	HELE    m_hEdit;
	USHORT  m_style1;
	HIMAGE  m_hImageAvatar;
	HIMAGE  m_hImageAvatar2;
	HIMAGE  m_hImageBubble;
	HIMAGE  m_hImageBubble2;
	void OnExit()
	{
		if (XC_IsHXCGUI(m_hImageAvatar, XC_IMAGE_FRAME)) XImage_Release(m_hImageAvatar);
		if (XC_IsHXCGUI(m_hImageAvatar2, XC_IMAGE_FRAME)) XImage_Release(m_hImageAvatar2);
		if (XC_IsHXCGUI(m_hImageBubble, XC_IMAGE_FRAME)) XImage_Release(m_hImageBubble);
		if (XC_IsHXCGUI(m_hImageBubble2, XC_IMAGE_FRAME)) XImage_Release(m_hImageBubble2);
	}
	void Init(HWINDOW hWindow, int left, int top, int width, int height)
	{
		m_hEdit = XEdit_CreateEx(left, top, width, height, edit_type_chat, hWindow);
		XEle_EnableKeyTab(m_hEdit, TRUE);
		XEdit_SetRowHeight(m_hEdit, 20);
		//XEle_SetPadding(m_hEdit,50, 0, 60, 0);

		HFONTX  hFont1 = XFont_Create2(L"微软雅黑", 9);
		m_style1 = XEdit_AddStyle(m_hEdit, hFont1, RGB(0, 0, 255), TRUE);
		m_hImageAvatar = XImage_LoadFile(L"image\\avatar.png"); //头像
		m_hImageAvatar2 = XImage_LoadFile(L"image\\logo.png"); //头像
		m_hImageBubble = XImage_LoadFileAdaptive(L"image\\bubble.png", 20, 15, 20, 15); //气泡
		m_hImageBubble2 = XImage_LoadFileAdaptive(L"image\\bubble2.png", 20, 16, 20, 10); //气泡2

// 		XEdit_AddText(m_hEdit, L"\n");
// 		XChat_AddChatBegin(m_hEdit, 0, 0, 0x4);
// 		XEdit_AddText(m_hEdit, L"系统消息:123456\n");
// 		XChat_AddChatEnd(m_hEdit);
	}
	void Recv(edit_data_copy_ *data)
	{
		{
			XEdit_AddText(m_hEdit, L"\n");
			XEdit_AddChatBegin(m_hEdit, 0, 0, 0x4);
			XEdit_AddText(m_hEdit, L"系统消息:123456\n");
			XEdit_AddChatEnd(m_hEdit);
		}

		static int send_left = 1;
		if (send_left % 2)
			XEdit_AddChatBegin(m_hEdit, m_hImageAvatar, m_hImageBubble, 0x1 | 0x8);
		else
			XEdit_AddChatBegin(m_hEdit, m_hImageAvatar2, m_hImageBubble2, 0x2 | 0x8);
		send_left++;
		XEdit_AddTextEx(m_hEdit, L"【实习】梦飞(154460336) 2019/1/23 20:30:12\n", m_style1);
		int iCurRow = XEdit_GetCurRow(m_hEdit);
		XEdit_SetRowHeightEx(m_hEdit, iCurRow - 1, 40);
		AddData(data);
		XEdit_AddChatEnd(m_hEdit);

		XEdit_AddText(m_hEdit, L"\n");
		XEdit_AutoScroll(m_hEdit);
		XEle_RedrawEle(m_hEdit);
	}
	void AddData(edit_data_copy_  *data)
	{
		//读取样式表
		vector<USHORT>  styleTable(data->nStyleCount, edit_style_no);
		for (int i = 0; i < data->nStyleCount; i++)
		{
			HIMAGE hObj = data->pStyle[i].hFont_image_obj;
			XC_OBJECT_TYPE ty = XObj_GetTypeBase(hObj);
			if (XC_IMAGE_FRAME == ty)
			{
				//HIMAGE hSrc=XImage_GetImageSrc(hObj);
				//const wchar_t* pFile = XImgSrc_GetFile(hSrc);
				int iStyle = XEdit_AddStyle(m_hEdit, hObj, 0, FALSE);
				styleTable[i] = iStyle;
			} else 	if (XC_FONT == ty)
			{
				COLORREF color = data->pStyle[i].color;
				int iStyle = XEdit_AddStyle(m_hEdit, (HFONTX)hObj, color, data->pStyle[i].bColor);
				styleTable[i] = iStyle;
			} else 	if (XC_ELE == ty)
			{
				if (XC_BUTTON == XC_GetObjectType(hObj))
				{
					HELE  hBtn = XBtn_Create(0, 0, XEle_GetWidth((HELE)hObj), XEle_GetHeight((HELE)hObj), XBtn_GetText((HELE)hObj), m_hEdit);
					int iStyle = XEdit_AddStyle(m_hEdit, hBtn, 0, FALSE);
					styleTable[i] = iStyle;
				}
			} else if (XC_SHAPE == ty)
			{
				if (XC_IsHXCGUI(hObj, XC_SHAPE_GIF))
				{
					HIMAGE hImageGif = XShapeGif_GetImage(hObj);
					//HIMAGE hSrc=XImage_GetImageSrc(hImageGif);
					//const wchar_t* pFile = XImgSrc_GetFile(hSrc);
					{
						HXCGUI hGif = XShapeGif_Create(0, 0, XImage_GetWidth(hImageGif), XImage_GetHeight(hImageGif), m_hEdit);
						XShapeGif_SetImage(hGif, hImageGif);
						int iStyle = XEdit_AddStyle(m_hEdit, hGif, 0, FALSE);
						styleTable[i] = iStyle;
					}
				}
			}
		}
		XEdit_AddData(m_hEdit, data, styleTable.data(), data->nStyleCount);
	}
};

class CEditSend
{
public:
	HWINDOW m_hWindow;
	HELE m_hEdit;
	HELE m_hBtnSend;
	HELE m_hBtnImg1;
	HELE m_hBtnImg2;
	HELE m_hBtnImg3;
	HELE m_hButton;
	HELE m_hBtnFont1;
	HELE m_hBtnFont2;
	HELE m_hBtnFont3;
	HELE m_hBtnColor1;
	HELE m_hBtnColor2;
	HELE m_hBtnColor3;
	int  m_iCurStyle;

	CEditRecv*   m_pRecv;
	void Init(HWINDOW hWindow, int left, int top, int width, int height)
	{
		m_hWindow = hWindow;
		InitBar(left, top);
		m_hBtnSend = XBtn_Create(left + width + 10, top, 80, 30, L"发 送", hWindow);
		XEle_EnableFocus(m_hBtnSend, FALSE);

		m_hEdit = XEdit_CreateEx(left, top, width, height, edit_type_richedit, hWindow);

		XEle_EnableKeyTab(m_hEdit, TRUE);
		XEdit_EnableAutoWrap(m_hEdit, FALSE);

		XEdit_AddText(m_hEdit, L"ABC");

		HIMAGE hImage1 = XImage_LoadFile(L"image\\123.png");
		HIMAGE hImage2 = XImage_LoadFile(L"image\\logo.png");
		XEdit_AddObject(m_hEdit, hImage1);
		XEdit_AddObject(m_hEdit, hImage2);

		HELE hBtn = XBtn_Create(0, 0, 60, 24, L"cc", m_hEdit);
		XEdit_AddObject(m_hEdit, hBtn);

		HIMAGE hImageGif = XImage_LoadFile(L"image\\gif.gif");
		if (hImageGif)
		{
			HXCGUI hGif = XShapeGif_Create(0, 0, XImage_GetWidth(hImageGif), XImage_GetHeight(hImageGif), m_hEdit);
			XShapeGif_SetImage(hGif, hImageGif);
			XEdit_AddObject(m_hEdit, hGif);
		}

		int iStyle = XEdit_AddStyleEx(m_hEdit, L"微软雅黑", 16, 0, RGB(200, 0, 0), TRUE);
		XEdit_AddTextEx(m_hEdit, L"123", iStyle);
		iStyle = XEdit_AddStyleEx(m_hEdit, L"微软雅黑", 24, 0, RGB(0, 200, 0), TRUE);
		XEdit_AddTextEx(m_hEdit, L"123", iStyle);
		iStyle = XEdit_AddStyleEx(m_hEdit, L"微软雅黑", 36, 0, RGB(0, 0, 200), TRUE);
		XEdit_AddTextEx(m_hEdit, L"123", iStyle);

		XEle_RegEventCPP(m_hBtnSend, XE_BNCLICK, &CEditSend::OnBtnClick_Send);
		XEle_RegEventCPP(m_hBtnImg1, XE_BNCLICK, &CEditSend::OnBtnClick_img1);
		XEle_RegEventCPP(m_hBtnImg2, XE_BNCLICK, &CEditSend::OnBtnClick_img2);
		XEle_RegEventCPP(m_hBtnImg3, XE_BNCLICK, &CEditSend::OnBtnClick_img3);
		XEle_RegEventCPP(m_hButton, XE_BNCLICK, &CEditSend::OnBtnClick_button);
		XEle_RegEventCPP(m_hBtnFont1, XE_BUTTON_CHECK, &CEditSend::OnBtnClick_font1);
		XEle_RegEventCPP(m_hBtnFont2, XE_BUTTON_CHECK, &CEditSend::OnBtnClick_font2);
		XEle_RegEventCPP(m_hBtnFont3, XE_BUTTON_CHECK, &CEditSend::OnBtnClick_font3);

		XEle_RegEventCPP(m_hBtnColor1, XE_BUTTON_CHECK, &CEditSend::OnBtnClick_color1);
		XEle_RegEventCPP(m_hBtnColor2, XE_BUTTON_CHECK, &CEditSend::OnBtnClick_color2);
		XEle_RegEventCPP(m_hBtnColor3, XE_BUTTON_CHECK, &CEditSend::OnBtnClick_color3);
		XEle_RegEventCPP(m_hEdit, XE_EDIT_STYLE_CHANGED, &CEditSend::OnEditStyleChanged);
		XWnd_RegEventCPP(hWindow, WM_PAINT, &CEditSend::OnWndDrawWindow);
	}
	void InitBar(int left, int top)
	{
		int x = left;
		m_hBtnImg1 = XBtn_Create(x, top - 25, 60, 20, L"img1", m_hWindow); x += 65;
		m_hBtnImg2 = XBtn_Create(x, top - 25, 60, 20, L"img2", m_hWindow); x += 65;
		m_hBtnImg3 = XBtn_Create(x, top - 25, 60, 20, L"gif", m_hWindow); x += 65;
		m_hButton = XBtn_Create(x, top - 25, 60, 20, L"button", m_hWindow); x += 65;
		m_hBtnFont1 = XBtn_Create(x, top - 25, 60, 20, L"字体12", m_hWindow); x += 65;
		m_hBtnFont2 = XBtn_Create(x, top - 25, 60, 20, L"字体24", m_hWindow); x += 65;
		m_hBtnFont3 = XBtn_Create(x, top - 25, 60, 20, L"字体36", m_hWindow); x += 65;

		m_hBtnColor1 = XBtn_Create(x, top - 25, 60, 20, L"color1", m_hWindow); x += 65;
		m_hBtnColor2 = XBtn_Create(x, top - 25, 60, 20, L"color2", m_hWindow); x += 65;
		m_hBtnColor3 = XBtn_Create(x, top - 25, 60, 20, L"color3", m_hWindow); x += 65;

		XEle_EnableFocus(m_hBtnImg1, FALSE);
		XEle_EnableFocus(m_hBtnImg2, FALSE);
		XEle_EnableFocus(m_hBtnImg3, FALSE);
		XEle_EnableFocus(m_hButton, FALSE);
		XEle_EnableFocus(m_hBtnFont1, FALSE);
		XEle_EnableFocus(m_hBtnFont2, FALSE);
		XEle_EnableFocus(m_hBtnFont3, FALSE);

		XEle_EnableFocus(m_hBtnColor1, FALSE);
		XEle_EnableFocus(m_hBtnColor2, FALSE);
		XEle_EnableFocus(m_hBtnColor3, FALSE);

		XBtn_SetType(m_hBtnFont1, button_type_radio);
		XBtn_SetType(m_hBtnFont2, button_type_radio);
		XBtn_SetType(m_hBtnFont3, button_type_radio);
		XBtn_SetGroupID(m_hBtnFont1, 2);
		XBtn_SetGroupID(m_hBtnFont2, 2);
		XBtn_SetGroupID(m_hBtnFont3, 2);

		XBtn_SetType(m_hBtnColor1, button_type_radio);
		XBtn_SetType(m_hBtnColor2, button_type_radio);
		XBtn_SetType(m_hBtnColor3, button_type_radio);
		XBtn_SetGroupID(m_hBtnColor1, 3);
		XBtn_SetGroupID(m_hBtnColor2, 3);
		XBtn_SetGroupID(m_hBtnColor3, 3);
	}

	int  OnBtnClick_Send(BOOL *pbHandled)
	{
		edit_data_copy_*  data = XEdit_GetData(m_hEdit);
		m_pRecv->Recv(data);
		XEdit_FreeData(data);
		return 0;
	}
	int  OnBtnClick_img1(BOOL *pbHandled)
	{
		HIMAGE hImage = XImage_LoadFile(L"image\\123.png");
		if (hImage)
		{
			XEdit_AddObject(m_hEdit, hImage);
			XEle_AdjustLayout(m_hEdit);

			XEdit_AutoScroll(m_hEdit);
			XEle_RedrawEle(m_hEdit);
		}
		return 0;
	}
	int  OnBtnClick_img2(BOOL *pbHandled)
	{
		HIMAGE hImage = XImage_LoadFile(L"image\\logo.png");
		if (hImage)
		{
			XEdit_AddObject(m_hEdit, hImage);
			XEle_AdjustLayout(m_hEdit);

			XEdit_AutoScroll(m_hEdit);
			XEle_RedrawEle(m_hEdit);
		}
		return 0;
	}
	int  OnBtnClick_img3(BOOL *pbHandled)
	{
		HIMAGE hImageGif = XImage_LoadFile(L"image\\gif.gif");
		if (hImageGif)
		{
			HXCGUI hGif = XShapeGif_Create(0, 0, XImage_GetWidth(hImageGif), XImage_GetHeight(hImageGif), m_hEdit);
			XShapeGif_SetImage(hGif, hImageGif);
			XEdit_AddObject(m_hEdit, hGif);
			XEle_AdjustLayout(m_hEdit);

			XEdit_AutoScroll(m_hEdit);
			XEle_RedrawEle(m_hEdit);
		}
		return 0;
	}
	int  OnBtnClick_button(BOOL *pbHandled)
	{
		HELE hButton = XBtn_Create(0, 0, 60, 20, L"button", m_hEdit);
		XEdit_AddObject(m_hEdit, hButton);
		XEle_AdjustLayout(m_hEdit);

		XEdit_AutoScroll(m_hEdit);
		XEle_RedrawEle(m_hEdit);
		return 0;
	}
	int  OnBtnClick_font1(BOOL bCheck, BOOL *pbHandled)
	{
		if (bCheck)	FontColorChange();
		return 0;
	}
	int  OnBtnClick_font2(BOOL bCheck, BOOL *pbHandled)
	{
		if (bCheck)	FontColorChange();
		return 0;
	}
	int  OnBtnClick_font3(BOOL bCheck, BOOL *pbHandled)
	{
		if (bCheck)	FontColorChange();
		return 0;
	}
	int  OnBtnClick_color1(BOOL bCheck, BOOL *pbHandled)
	{
		if (bCheck)	FontColorChange();
		return 0;
	}
	int  OnBtnClick_color2(BOOL bCheck, BOOL *pbHandled)
	{
		if (bCheck)	FontColorChange();
		return 0;
	}
	int  OnBtnClick_color3(BOOL bCheck, BOOL *pbHandled)
	{
		if (bCheck) FontColorChange();
		return 0;
	}
	void FontColorChange()
	{
		int fontSize = 12;
		if (XBtn_IsCheck(m_hBtnFont1))
		{
			fontSize = 12;
		} else if (XBtn_IsCheck(m_hBtnFont2))
		{
			fontSize = 24;
		} else if (XBtn_IsCheck(m_hBtnFont3))
		{
			fontSize = 36;
		}
		COLORREF  color = 0;
		if (XBtn_IsCheck(m_hBtnColor1))
		{
			color = RGB(200, 0, 0);
		} else if (XBtn_IsCheck(m_hBtnColor2))
		{
			color = RGB(0, 200, 0);
		} else if (XBtn_IsCheck(m_hBtnColor3))
		{
			color = RGB(0, 0, 200);
		}
		int iStyle = XEdit_AddStyleEx(m_hEdit, L"微软雅黑", fontSize, 0, color, TRUE);
		m_iCurStyle = iStyle;
		XEdit_SetCurStyle(m_hEdit, iStyle);
		XWnd_RedrawWnd(m_hWindow);
	}
	int  OnEditStyleChanged(int iStyle, BOOL *pbHandled)
	{
		m_iCurStyle = iStyle;
		XWnd_RedrawWnd(m_hWindow);
		return 0;
	}
	int  OnWndDrawWindow(HDRAW hDraw, BOOL *pbHandled)
	{
		*pbHandled = TRUE;
		XWnd_DrawWindow(m_hWindow, hDraw);
		RECT rc;
		XEle_GetRect(m_hEdit, &rc);
		XDraw_SetBrushColor(hDraw, RGB(200, 0, 0));

		edit_style_info_ info;
		if (XEdit_GetStyleInfo(m_hEdit, m_iCurStyle, &info))
		{
			if (edit_style_type_font_color == info.type)
			{
				if (info.hFont_image_obj)
				{
					wstring  text = L"字体:";
					xc_font_info_i f;
					XFont_GetFontInfo((HFONTX)info.hFont_image_obj, &f);
					text += f.name;
					text += L", ";
					text += XC_itow(f.nSize);
					XDraw_TextOut(hDraw, rc.right + 10, rc.top + 50, text.c_str(), text.size());
				};
				if (info.bColor)
				{
					wstring text = L"颜色:";
					wchar_t  buf[32] = { 0 };
					buf[0] = L'#';
					buf[1] = L'F';
					buf[2] = L'F';
					wsprintf(buf+3, L"%02X", GetRValue(info.color));
					wsprintf(buf+5, L"%02X", GetGValue(info.color));
					wsprintf(buf+7, L"%02X", GetBValue(info.color));
					text += buf;
					XDraw_TextOut(hDraw, rc.right + 10, rc.top + 75, text.c_str(), text.size());
				}
			}
		}
		return 0;
	}
};

class CQQChat
{
public:
	HWINDOW m_hWindow;
	HELE m_hEdit;
	CEditRecv   m_edit_recv;
	CEditSend   m_edit_send;

	CQQChat() {
		Init();
	}
	void Init()
	{
		m_hWindow = XWnd_Create(0, 0, 800, 800, L"炫彩界面库窗口", NULL, xc_window_style_default);
		HELE m_hButton_close = XBtn_Create(400, 5, 60, 20, L"close", m_hWindow);
		XBtn_SetType(m_hButton_close, button_type_close);
		XWnd_EnableDragWindow(m_hWindow, TRUE);
		XWnd_EnableDragBorder(m_hWindow, FALSE);

		int top = 40;
		m_edit_recv.Init(m_hWindow, 20, top, 600, 500); top += (500 + 30);
		m_edit_send.Init(m_hWindow, 20, top, 600, 200);
		m_edit_send.m_pRecv = &m_edit_recv;
		XWnd_ShowWindow(m_hWindow, SW_SHOW);
	}
	void OnExit()
	{
		m_edit_recv.OnExit();
	}
};

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	XInitXCGUI();
	CQQChat  MyWindow;
	XRunXCGUI();
	MyWindow.OnExit();
	XExitXCGUI();
	return 0;
}
