def 整型 入口函数_窗口()   //窗口程序入口
	//文本A rawJson =A"{\"Age\": 20, \"Name\": \"colin\", \"obj\":{\"a\":1, \"b\":2}}"
	文本A rawJson =AR"({"Age": 20, "Name": "colin", "obj":{"a":1, "b":2}})"
	//根节点
	CJsonValue     root
	CJsonReader  reader //读取解析JSON
	如果  !reader.解析(rawJson, root)
		返回 0
	文本A  ret_name = root.取文本(A"Name")
	文本A  ret_age = root.取文本(A"Age")
	调试输出A(ret_name)
	调试输出A(ret_age)
	
	CJsonValue obj = root.取对象(A"obj")
	整型 a =obj.取整型(A"a")
	整型 b =obj.取整型(A"b")
	调试输出A(a)
	调试输出A(b)
	
	无符号整型  size =  obj.取成员数()
	
	返回 0
