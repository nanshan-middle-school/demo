#函数类型  整型(__stdcall *fun_test5)(整型,整型) 
def   整型 __stdcall Add(整型 a, 整型 b=-1)
	调试输出("add()")
	返回  a+b

类  CA123
	静态 常量 整型  _a=1
	常量  静态  短整型  _成员变量
	fun_test5  _fun
	def  静态  整型  Test()
		返回 10
	def   整型 CALLBACK Add(整型 a, 整型 b=-1)
		调试输出("add()")
		返回  a+b
	
def 整型 入口函数_窗口()   //窗口程序入口
	CA123  ca
	ca.Test()
	ca._fun
	ca._fun()
	
	整型  a =  10 ^ 1
	Add(a*10, a*10) 
	fun_test5 fun = Add
	fun( CA123::_a, CA123::Test()*10)
	
	返回 0
