#define  窗口句柄   HWINDOW
#define  元素句柄   HELE

//窗口类接口
类 窗口
	//窗口句柄
	窗口句柄  _句柄
	
	//创建窗口
	def 窗口句柄 创建(整型 x, 整型 y, 整型 cx, 整型 cy,  文本 title, HWND hWndParent=0, 整型 XCStyle=xc_window_style_default)
		_句柄 = XWnd_Create(x, y,cx,cy, title, hWndParent, XCStyle)
		//123
		返回 _句柄
	
	//调整布局
	def 无类型 调整布局()
		XWnd_AdjustLayout(_句柄)
	
	//显示
	def 布尔型 显示(整型 nCmdShow)
		返回 XWnd_ShowWindow(_句柄, nCmdShow)

//按钮类接口
类 按钮
	//按钮句柄
	元素句柄  _句柄
	
	//创建按钮
	def 元素句柄 创建(整型 x, 整型 y, 整型 cx, 整型 cy, 文本 title, HXCGUI hParent)
		_句柄= XBtn_Create(x,y,cx,cy, title, hParent)
		返回 _句柄
	
	//设置按钮类型
	def 无类型 置类型2(XC_OBJECT_TYPE_EX nType)
		XBtn_SetTypeEx(_句柄, nType)

