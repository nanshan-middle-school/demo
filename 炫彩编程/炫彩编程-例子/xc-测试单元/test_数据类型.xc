def 整型 测试_数据类型()
	整型    type_int= 1000
	短整型  type_short=100
	字节型  type_byte=10
	浮点型  type_float=3.14	
	整型    type_array_int[10]={0}
	文本    type_text = "123-ABC"
	
	调试输出("测试_数据类型 整型: ", type_int, "  大小:",sizeof(type_int))
	调试输出("测试_数据类型 短整型: ", type_short, "  大小:",sizeof(type_short))
	调试输出("测试_数据类型 字节型: ", type_byte, "  大小:",sizeof(type_byte))
	调试输出("测试_数据类型 浮点型: ", type_float, "  大小:",sizeof(type_float))
	调试输出("测试_数据类型 数组: ", type_array_int, "  大小:",sizeof(type_array_int))	
	调试输出("测试_数据类型 文本: ", type_text, "  大小:",sizeof(type_text))

	返回 1
