def 无类型  测试_字典()
	test_map1()
	test_map2()
	test_map3()

def 无类型 test_map1_print(字典 < 文本 , 整型 >   map_a) 
	整型  a=map_a["aaa"]
	整型  数量 = map_a.size()
	pass

def 整型 test_map1() 
	字典 < 文本 , 整型 >   map_a
	map_a[ "aaa" ]= 10
	map_a[ "bbb" ]=20
	map_a[ "ccc" ]= 30
	整型  a=map_a["aaa"]
	
	整型  数量 = map_a.大小()
	迭代器  迭代器_a=map_a.查找("aaa")
	
	如果 迭代器_a==map_a.空值()
		调试输出("未找到")
	否则
		调试输出("找到")
	
	列表循环 迭代器  迭代器_b; map_a
		调试输出("key: " , 迭代器_b.键, "  value: " ,迭代器_b.值 )
		
	test_map1_print(map_a)
	返回 0

def 整型 test_map2() 
	字典 < wchar_t* , 整型 >   map_a
	map_a[ "aaa" ]= 10
	map_a[ "bbb" ]=20
	map_a[ "ccc" ]= 30
	整型  a=map_a["aaa"]
	
	整型  数量 = map_a.size()
	迭代器  迭代器_a=map_a.find("aaa")
	如果 迭代器_a==map_a.end()
		调试输出("未找到")
	否则
		调试输出("找到")
	
	列表循环 迭代器  迭代器_b; map_a
		调试输出("key: " , 迭代器_b.键, "  value: " , 迭代器_b.值 )
	返回 0
	
def 无类型  test_map3()
	字典 < 整型 , 整型 >   map_a
	map_a[ 1 ]= 10
	map_a[ 2 ]=20
	map_a[ 3 ]= 30
	整型  a=map_a[2]
	
	整型  数量 = map_a.size()
	迭代器  迭代器_a=map_a.find(1)
	如果 迭代器_a==map_a.end()
		调试输出("未找到")
	否则
		调试输出("找到")
	
	列表循环 迭代器  迭代器_b; map_a
		调试输出("key: " , 迭代器_b.键, "  value: " , 迭代器_b.值 )
