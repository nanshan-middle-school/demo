
def RECT GetRect()
	RECT rc
	rc.left=10
	返回  rc

def 无类型  SetRect(RECT rc)
	rc.left=20

def 无类型  SetRectPtr(RECT*  pRc)
	pRc->left=30

def 整型 测试_函数传递参数()
	调试输出("测试_函数传递参数")
	
	RECT rc = GetRect()
	调试输出(rc.left)
	
	RECT*  pRc = &rc
	调试输出(pRc->left)
	
	SetRect(rc)
	调试输出(rc.left)
	
	SetRectPtr(pRc)
	调试输出(pRc->left)
	
	返回 0
