
类  CA1
	def 虚函数 整型  Add(整型 a, 整型 b)
		调试输出("call CA::Add()")
		返回 a+b

类  CB1 继承 CA1
	def 虚函数 整型  Add(整型 a, 整型 b)
		调试输出("call CB::Add()")
		整型 c = CA1::Add(a, b) //调用父类Add()
		返回 a+b+c
		
def 无类型  测试_类虚函数()
	调试输出("测试_类虚函数")
	CB1   cb
	整型 ret = cb.Add(1,2)
	
	CA1*  ca = &cb
	整型 ret2 = ca->Add(1,2)
	
	调试输出(ret)
	调试输出(ret2)