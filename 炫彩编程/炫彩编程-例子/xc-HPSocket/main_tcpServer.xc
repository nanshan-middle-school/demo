类 CTcpServerListenerImpl  继承 CTcpServerListener
	def 虚函数 EnHandleResult OnPrepareListen(ITcpServer* pSender, SOCKET soListen) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnAccept(ITcpServer* pSender, CONNID dwConnID, UINT_PTR soClient)  重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnHandShake(ITcpServer* pSender, CONNID dwConnID)
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnReceive(ITcpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnReceive(ITcpServer* pSender, CONNID dwConnID, int iLength)  重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnSend(ITcpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnShutdown(ITcpServer* pSender)
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnClose(ITcpServer* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode)  重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE

def 整型 测试_tcpServer()
	CTcpServerListenerImpl  listener
	CTcpServerPtr  tcpServer(&listener)
	如果  tcpServer->Start("127.0.0.0", 80)
		循环 1
			字符型A  ch = getchar()
			如果  A'c'==ch
				调试输出("停止")
				tcpServer->Stop()
				跳出
	否则
		调试输出("失败: ",tcpServer->GetLastError(), tcpServer->GetLastErrorDesc())
	返回 0
