类 CHttpServerListenerImpl  继承 CHttpServerListener
	def 虚函数 EnHandleResult OnPrepareListen(ITcpServer* pSender, SOCKET soListen) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnAccept(ITcpServer* pSender, CONNID dwConnID, UINT_PTR soClient)  重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnHandShake(ITcpServer* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnReceive(ITcpServer* pSender, CONNID dwConnID, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnReceive(ITcpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnSend(ITcpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnShutdown(ITcpServer* pSender)
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnClose(ITcpServer* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	
	def 虚函数 EnHttpParseResult OnMessageBegin(IHttpServer* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnRequestLine(IHttpServer* pSender, CONNID dwConnID, const char* lpszMethod,  const char* lpszUrl) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnStatusLine(IHttpServer* pSender, CONNID dwConnID, USHORT usStatusCode,  const char* lpszDesc) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnHeader(IHttpServer* pSender, CONNID dwConnID,  const char* lpszName,  const char* lpszValue) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnChunkHeader(IHttpServer* pSender, CONNID dwConnID, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnChunkComplete(IHttpServer* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnUpgrade(IHttpServer* pSender, CONNID dwConnID, EnHttpUpgradeType enUpgradeType) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK

	//def 虚函数 EnHandleResult OnWSMessageHeader(IHttpServer* pSender, CONNID dwConnID, BOOL bFinal, BYTE iReserved, BYTE iOperationCode, const BYTE lpszMask[4], ULONGLONG ullBodyLen) { return HR_IGNORE; }
	def 虚函数 EnHandleResult OnWSMessageBody(IHttpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnWSMessageComplete(IHttpServer* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	
	def 虚函数 EnHttpParseResult OnHeadersComplete(IHttpServer* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnMessageComplete(IHttpServer* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnParseError(IHttpServer* pSender, CONNID dwConnID, int iErrorCode, const  char*  lpszErrorDesc) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK
	def 虚函数 EnHttpParseResult OnBody(IHttpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HPR_OK

def 整型 测试_httpServer()
	CHttpServerListenerImpl  listener
	CHttpServerPtr  httpServer(&listener)
	如果  httpServer->Start("134.175.30.97", 80)
		循环 1
			字符型A  ch = getchar()
			如果  A'c'==ch
				调试输出("停止")
				httpServer->Stop()
				跳出
	否则
		调试输出("失败: ",httpServer->GetLastError(), httpServer->GetLastErrorDesc())
	返回 0

