类 CHttpClientListenerImpl  继承  CHttpClientListener
	def 虚函数 EnHandleResult   OnPrepareConnect(ITcpClient* pSender, CONNID dwConnID, SOCKET socket) 重写
		调试输出("OnPrepareConnect()")
		返回 HR_OK
	def 虚函数 EnHandleResult OnConnect(ITcpClient* pSender, CONNID dwConnID) 重写
		调试输出("OnConnect()")
		返回 HR_OK
	def 虚函数 EnHandleResult OnHandShake(ITcpClient* pSender, CONNID dwConnID) 重写
		调试输出("OnHandShake()")
		返回 HR_OK
	def 虚函数 EnHandleResult OnSend(ITcpClient* pSender, CONNID dwConnID,  const BYTE* pData, int iLength) 重写
		调试输出("OnSend() iLength:", iLength)
		返回 HR_OK
	def 虚函数 EnHttpParseResult OnMessageBegin(IHttpClient* pSender, CONNID dwConnID) 重写
		调试输出("OnMessageBegin()")
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnRequestLine(IHttpClient* pSender, CONNID dwConnID,  const char* lpszMethod,  const char* lpszUrl) 重写
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnStatusLine(IHttpClient* pSender, CONNID dwConnID, USHORT usStatusCode,  const char* lpszDesc) 重写
		调试输出("OnStatusLine()")
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnHeader(IHttpClient* pSender, CONNID dwConnID,  const char* lpszName,  const char* lpszValue) 重写
		调试输出A(A"OnHeader()   ", lpszName, lpszValue)
		返回 HPR_OK		
	def 虚函数 EnHttpParseResult OnChunkHeader(IHttpClient* pSender, CONNID dwConnID, int iLength) 重写
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnChunkComplete(IHttpClient* pSender, CONNID dwConnID) 重写
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnUpgrade(IHttpClient* pSender, CONNID dwConnID, EnHttpUpgradeType enUpgradeType) 重写
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnParseError(IHttpClient* pSender, CONNID dwConnID, int iErrorCode, const  char* lpszErrorDesc) 重写
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnHeadersComplete(IHttpClient* pSender, CONNID dwConnID) 重写
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnBody(IHttpClient* pSender, CONNID dwConnID,  const BYTE* pData, int iLength) 重写
		调试输出("OnBody() iLength:", iLength)
		char buf[20480] = {0};
		memcpy(buf, pData, iLength);
		buf[iLength] = 0;
		调试输出A(A"OnBody():  ", buf)
		返回 HPR_OK
	def 虚函数 EnHttpParseResult OnMessageComplete(IHttpClient* pSender, CONNID dwConnID) 重写
		返回 HPR_OK
	def 虚函数 EnHandleResult OnWSMessageBody(IHttpClient* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		返回 HR_IGNORE
	def 虚函数 EnHandleResult OnWSMessageComplete(IHttpClient* pSender, CONNID dwConnID) 重写
		返回 HR_IGNORE
	def 虚函数 EnHandleResult OnClose(ITcpClient* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode) 重写
		调试输出("OnClose()")
		返回 HR_IGNORE

def 整型 测试_httpClient()
	CHttpClientListenerImpl  listener
	CHttpClientPtr  httpClient(&listener)
	如果  httpClient->Start("134.175.30.97", 80, FALSE)
		TNVPair  header[10]
		header[0].name = A"Accept"
		header[0].value = A"text/html"

		header[1].name = A"Host"
		header[1].value = A"www.xcgui.com"

		如果 httpClient->SendRequest(A"GET", A"test/", header, 2)
			调试输出("---SendRequest---")
		Sleep(2000)
		httpClient->Stop()
	否则
		调试输出("失败: ",httpClient->GetLastError(), httpClient->GetLastErrorDesc())
	返回 0

