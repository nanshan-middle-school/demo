类 CTcpClientListenerImpl 继承 CTcpClientListener
	def 虚函数 EnHandleResult OnPrepareConnect(ITcpClient* pSender, CONNID dwConnID, SOCKET socket)  重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnConnect(ITcpClient* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnHandShake(ITcpClient* pSender, CONNID dwConnID) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnReceive(ITcpClient* pSender, CONNID dwConnID, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnSend(ITcpClient* pSender, CONNID dwConnID, const BYTE* pData, int iLength)  重写
		调试输出("OnSend() iLength:", iLength)
		char buf[20480] = {0}
		memcpy(buf, pData, iLength)
		buf[iLength] = 0
		调试输出A(A"OnBody():  ", buf)
		返回 HR_IGNORE
	def 虚函数 EnHandleResult OnReceive(ITcpClient* pSender, CONNID dwConnID, const BYTE* pData, int iLength) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE
	def 虚函数 EnHandleResult OnClose(ITcpClient* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode) 重写
		调试输出(__FUNCTION__); 返回 HR_IGNORE

def 整型 测试_tcpClient()
	CTcpClientListenerImpl  listener
	CTcpClientPtr  tcpClient(&listener)
	如果  tcpClient->Start("134.175.30.97", 80, FALSE)
		const char buf[] = A"123456"
		int len = strlen(buf)
		如果 tcpClient->Send((BYTE*)buf, len,0)
			调试输出("---Send---")
		Sleep(2000)
		tcpClient->Stop()
	否则
		调试输出("失败: ",tcpClient->GetLastError(), tcpClient->GetLastErrorDesc())
	返回 0
	