def 整型 回调   OnProgressBarChange(整型 pos,布尔型 *pbHandled)
	调试输出("OnProgressBarChange(): ", pos)
	返回 0

进度条类  进度条
def 整型 回调 OnBtnClick_加(BOOL *pbHandled)
	进度条.置进度(进度条.取进度()+1)
	进度条.重绘()
	返回 0

def 整型 回调 OnBtnClick_减(BOOL *pbHandled)
	进度条.置进度(进度条.取进度()-1)
	进度条.重绘()
	返回 0

def 整型 入口函数_窗口()
	炫彩_初始化()       //初始化界面库
	窗口类 主窗口(0,0, 300, 300, "xcgui-   window", NULL, xc_window_style_default)//创建窗口
	如果 主窗口.句柄
		按钮类 关闭按钮(10,5,80,25,"关 闭", 主窗口.句柄) //创建关闭窗口按钮
		关闭按钮.置类型扩展( button_type_close)   //设置按钮类型, 关闭窗口
		
		进度条.创建(20,50, 200, 20, 主窗口.句柄)
		进度条.置水平(TRUE)
		进度条.置范围(10)
		进度条.置进度(5)
		进度条.置两端大小(10,10)
		进度条.注册事件C(XE_PROGRESSBAR_CHANGE, OnProgressBarChange)
		
		按钮类  按钮加(20,80,50,20,"+",主窗口.句柄)
		按钮类  按钮减(80,80,50,20,"-",主窗口.句柄)
		按钮加.注册事件C(XE_BNCLICK, OnBtnClick_加)
		按钮减.注册事件C(XE_BNCLICK, OnBtnClick_减)
		
		主窗口.调整布局() //调整布局
		主窗口.显示(SW_SHOW)  //显示窗口
		炫彩_运行()
	炫彩_退出()
	返回 0
	