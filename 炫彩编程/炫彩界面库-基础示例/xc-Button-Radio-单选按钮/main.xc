def 整型 回调 OnBtnCheck(HELE  hButton, BOOL  bCheck, BOOL *pbHandled)
	如果 bCheck
		调试输出("OnBtnCheck()", 按钮_取文本(hButton))
	返回 0
	
def 整型 入口函数_窗口() 
	炫彩_初始化()       //初始化界面库
	窗口类 主窗口(0,0, 300, 300, "xcgui-   window", NULL, xc_window_style_default)//创建窗口
	如果 主窗口.句柄
		按钮类 关闭按钮(10,5,80,25,"关 闭", 主窗口.句柄) //创建关闭窗口按钮
		关闭按钮.置类型扩展( button_type_close)   //设置按钮类型, 关闭窗口

		按钮类  单选按钮1(20,50, 100, 22, "单选按钮1", 主窗口.句柄) 
		按钮类  单选按钮2(20,80, 100, 22, "单选按钮2", 主窗口.句柄) 
		按钮类  单选按钮3(20,110, 100, 22, "单选按钮3", 主窗口.句柄) 
		
		单选按钮1.置组ID(1)  //设置分组ID
		单选按钮2.置组ID(1)
		单选按钮3.置组ID(1)
		
		单选按钮1.置类型扩展(button_type_radio) //设置为单选按钮
		单选按钮2.置类型扩展(button_type_radio)
		单选按钮3.置类型扩展(button_type_radio)
		
		单选按钮1.注册事件C1(XE_BUTTON_CHECK, OnBtnCheck) //注册选中事件
		单选按钮2.注册事件C1(XE_BUTTON_CHECK, OnBtnCheck)
		单选按钮3.注册事件C1(XE_BUTTON_CHECK, OnBtnCheck)
		
		主窗口.调整布局() //调整布局
		主窗口.显示(SW_SHOW)  //显示窗口
		炫彩_运行()
	炫彩_退出()
	返回 0

