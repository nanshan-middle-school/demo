def 整型 回调 OnScrollViewScrollH(int pos, BOOL *pbHandled)
	调试输出("OnScrollViewScrollH()", pos )
	返回 0

def 整型 回调 OnScrollViewScrollV(int pos, BOOL *pbHandled)
	调试输出("OnScrollViewScrollV()", pos )
	返回 0

def 整型 入口函数_窗口()
	炫彩_初始化()       //初始化界面库
	窗口类 主窗口(0,0, 300, 300, "xcgui-   window", NULL, xc_window_style_default)//创建窗口
	如果 主窗口.句柄
		按钮类 关闭按钮(10,5,80,25,"关 闭", 主窗口.句柄) //创建关闭窗口按钮
		关闭按钮.置类型扩展( button_type_close)   //设置按钮类型, 关闭窗口

		滚动视图类  滚动视图(20,50, 200, 200, 主窗口.句柄)
		滚动视图.置视图大小(300,300)
		
		按钮类  按钮1(20,10, 100,20, "button1", 滚动视图.句柄)
		按钮类  按钮2(20,40, 100,20, "button2" ,滚动视图.句柄)
		按钮类  按钮3(20,70, 100,20, "button3", 滚动视图.句柄)
		
		滚动视图.注册事件C(XE_SCROLLVIEW_SCROLL_H, OnScrollViewScrollH) //注册事件 水平滚动
		滚动视图.注册事件C(XE_SCROLLVIEW_SCROLL_V, OnScrollViewScrollV) //注册事件 垂直滚动
		
		主窗口.调整布局() //调整布局
		主窗口.显示(SW_SHOW)  //显示窗口
		炫彩_运行()
	炫彩_退出()
	返回 0

