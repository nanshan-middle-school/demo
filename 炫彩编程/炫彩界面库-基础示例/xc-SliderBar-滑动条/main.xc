def 整型 回调   OnSliderBarChange(整型 pos, 布尔型 *pbHandled)
	调试输出("OnSliderBarChange(): ", pos)
	返回 0

def 整型 入口函数_窗口()
	炫彩_初始化()       //初始化界面库
	窗口类 主窗口(0,0, 300, 300, "xcgui-   window", NULL, xc_window_style_default)//创建窗口
	如果 主窗口.句柄
		按钮类 关闭按钮(10,5,80,25,"关 闭", 主窗口.句柄) //创建关闭窗口按钮
		关闭按钮.置类型扩展( button_type_close)   //设置按钮类型, 关闭窗口
		
		滑动条类 滑动条(20,50, 200,30, 主窗口.句柄)
		滑动条.置水平(TRUE)
		滑动条.置滑块宽度(10)
		滑动条.置滑块高度(20)
		滑动条.置范围(10)
		滑动条.置当前位置(5)
		滑动条.置两端大小(10,10)
		滑动条.注册事件C(XE_SLIDERBAR_CHANGE, OnSliderBarChange)
		
		主窗口.调整布局() //调整布局
		主窗口.显示(SW_SHOW)  //显示窗口
		炫彩_运行()
	炫彩_退出()
	返回 0
	