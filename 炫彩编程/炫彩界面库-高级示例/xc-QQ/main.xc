
类  CQQ
	HWINDOW m_hWindow
	HELE     m_hTree
	HTEMP  m_hTemplate_group
	HIMAGE m_hExpand
	HIMAGE m_hExpandNo
	HIMAGE m_hVip
	HIMAGE m_hQZone
	HIMAGE m_hAvatarLarge
	HIMAGE m_hAvatarSmall
	
	def HWINDOW Init()
		BOOL bRes=XC_LoadResource("resource.res")
		m_hWindow=(HWINDOW)XC_LoadLayout("main.xml",0)
		
		m_hTree=(HELE)XC_GetObjectByName("tree")
		XTree_EnableConnectLine(m_hTree,FALSE,FALSE)
		XSView_ShowSBarH(m_hTree,FALSE)
		XTree_SetIndentation(m_hTree,0)
		XTree_SetItemHeightDefault(m_hTree,28,54)
		
		XTree_SetItemTemplateXML(m_hTree,"xml-template\\Tree_Item_friend.xml")
		XTree_SetItemTemplateXMLSel(m_hTree,"xml-template\\Tree_Item_friend_sel.xml")
		m_hTemplate_group=XTemp_Load(listItemTemp_type_tree,"xml-template\\Tree_Item_group.xml")
		
		m_hVip=XImage_LoadFile("image\\SuperVIP_LIGHT.png",FALSE)
		m_hQZone=XImage_LoadFile("image\\QQZone.png",FALSE)
		m_hAvatarSmall=XImage_LoadFile("image\\avatar_small.png",FALSE)
		m_hAvatarLarge=XImage_LoadFile("image\\avatar_large.png",FALSE)
		
		m_hExpand=XImage_LoadFile("image\\expand.png",FALSE)
		m_hExpandNo=XImage_LoadFile("image\\expandno.png",FALSE)
		XImage_EnableAutoDestroy(m_hExpandNo,FALSE)
		XImage_EnableAutoDestroy(m_hExpand,FALSE)
		
		HXCGUI hAdapter=XAdTree_Create()
		XTree_BindAdapter(m_hTree, hAdapter)
		XAdTree_AddColumn(hAdapter,"name1")
		XAdTree_AddColumn(hAdapter,"name2")
		XAdTree_AddColumn(hAdapter,"name3")
		XAdTree_AddColumn(hAdapter,"name4")
		XAdTree_AddColumn(hAdapter,"name5")
		XAdTree_AddColumn(hAdapter,"name6")
		
		整型 nGroupID=0
		整型 nItemID=0
		计次循环 整型 iGroup=0 ;3
			文本 text = mkStr("好友分组 - ", iGroup)
			nGroupID=XAdTree_InsertItemText(hAdapter,"123",XC_ID_ROOT,XC_ID_LAST)
			XTree_SetItemHeight(m_hTree,nGroupID,26,26)
			计次循环  整型 i=0; 5
				text = mkStr("我的好友-", i)
				nItemID=XAdTree_InsertItemText(hAdapter,"123",nGroupID,XC_ID_LAST)
				XAdTree_SetItemTextEx(hAdapter,nItemID,"name2","我的个性签签名")
				XAdTree_SetItemImageEx(hAdapter,nItemID,"name5",m_hVip)
				XAdTree_SetItemImageEx(hAdapter,nItemID,"name6",m_hQZone)
				XAdTree_SetItemImageEx(hAdapter,nItemID,"name3",m_hAvatarSmall)
				XAdTree_SetItemImageEx(hAdapter,nItemID,"name4",m_hAvatarLarge)
		
		XEle_RegEventCPP(m_hTree, XE_TREE_TEMP_CREATE, &CQQ::OnTemplateCreate) 
		XEle_RegEventCPP(m_hTree, XE_TREE_TEMP_CREATE_END, &CQQ::OnTreeTemplateCreateEnd)
		XEle_RegEventCPP(m_hTree, XE_DESTROY, &CQQ::OnDestroy)

		XWnd_AdjustLayout(m_hWindow)
		XWnd_ShowWindow(m_hWindow,SW_SHOW)
		返回 m_hWindow

	def 整型 OnTemplateCreate(tree_item_i* pItem, BOOL* pbHandled)
		调试输出("OnTemplateCreate()")
		如果 XC_ID_ERROR!=XTree_GetFirstChildItem(m_hTree,pItem->nID)
			如果 m_hTemplate_group
				调试输出("m_hTemplate_group:", (int)m_hTemplate_group)
				pItem->hTemp= m_hTemplate_group
		调试输出("=====测试修改值====pbHandled===>>100")
		*pbHandled=100
		返回 0
	
	def 整型 OnTreeTemplateCreateEnd(tree_item_i* pItem, BOOL* pbHandled)
		调试输出("OnTreeTemplateCreateEnd():", pItem->nID)
		HELE hButtonExpand= (HELE)XTree_GetTemplateObject(m_hTree, pItem->nID, 1)
		如果 hButtonExpand && XC_IsHELE(hButtonExpand)
			如果 m_hExpandNo
				XBtn_AddBkImage(hButtonExpand,button_state_leave, m_hExpandNo)
				XBtn_AddBkImage(hButtonExpand,button_state_stay, m_hExpandNo)
				XBtn_AddBkImage(hButtonExpand,button_state_down, m_hExpandNo)
			
			如果 m_hExpand
				XBtn_AddBkImage(hButtonExpand,button_state_check, m_hExpand)
			
			XEle_EnableBkTransparent(hButtonExpand, TRUE)
			XBtn_SetStyle(hButtonExpand, button_style_default)
			XEle_EnableFocus(hButtonExpand, FALSE)
		返回 0

	def 整型 OnDestroy(BOOL* pbHandled)
		调试输出("OnDestroy()")
		如果 m_hTemplate_group; XTemp_Destroy(m_hTemplate_group)
		如果 m_hExpand; XImage_Destroy(m_hExpand)
		如果 m_hExpandNo; XImage_Destroy(m_hExpandNo)
		如果 m_hVip; XImage_Destroy(m_hVip)
		如果 m_hQZone; XImage_Destroy(m_hQZone)
		如果 m_hAvatarSmall; XImage_Destroy(m_hAvatarSmall)
		如果 m_hAvatarLarge; XImage_Destroy(m_hAvatarLarge)
		返回 0

def 整型 入口函数_窗口()   //窗口程序入口
	XInitXCGUI()       //初始化界面库
	CQQ  qq
	qq.Init()
	XRunXCGUI()
	XExitXCGUI()
	返回 0
