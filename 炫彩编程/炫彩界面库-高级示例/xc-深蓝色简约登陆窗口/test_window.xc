HWINDOW hWindow=(HWINDOW)99  //窗口句柄
HELE    hLayoutContent=0  //布局内容
HELE    hCurPanel=0    //当前面板

def 无类型 SwitchTab(文本 strTy)
	如果 hCurPanel
		XEle_Destroy(hCurPanel)
	如果 "登陆"==strTy
		hCurPanel = (HELE)XC_LoadLayout("panel-login.xml",hLayoutContent)
		XEle_SetTextColor((HELE)XC_GetObjectByName("登陆"),0xFFFFFF,255)
		XEle_SetTextColor((HELE)XC_GetObjectByName("注册"),0x000000,255)
	否则
		hCurPanel = (HELE)XC_LoadLayout("panel-reg.xml",hLayoutContent)
		XEle_SetTextColor((HELE)XC_GetObjectByName("登陆"),0x000000,255)
		XEle_SetTextColor((HELE)XC_GetObjectByName("注册"),0xFFFFFF,255)
	XWnd_AdjustLayout(hWindow)
	XWnd_RedrawWnd(hWindow, 0)

def  整型 回调 OnBtnCheck_login(整型 bCheck, 整型 *pbHandled)
	如果 bCheck
		调试输出("按钮-登陆")
		SwitchTab("登陆")
	返回 0

def  整型 回调 OnBtnCheck_register(整型 bCheck, 整型 *pbHandled)
	如果 bCheck
		调试输出("按钮-注册")
		SwitchTab("注册")
	返回 0

def 整型 入口函数_窗口()   //窗口程序入口
	XInitXCGUI()       //初始化界面库
	
	XC_LoadResource("resource.res")
	hWindow = (HWINDOW)XC_LoadLayout("main.xml",0) //创建窗口
	如果 hWindow
		hLayoutContent =(HELE)XC_GetObjectByName("content")
		SwitchTab("登陆")
		XEle_RegEventC((HELE)XC_GetObjectByName("登陆"), XE_BUTTON_CHECK, OnBtnCheck_login)
		XEle_RegEventC((HELE)XC_GetObjectByName("注册"), XE_BUTTON_CHECK, OnBtnCheck_register)
		
		XWnd_AdjustLayout(hWindow) //调整布局
		XWnd_ShowWindow(hWindow,SW_SHOW) //显示窗口
		XRunXCGUI()
	XExitXCGUI()
	返回 0
