
类  CQQ   : 继承   窗口类
	列表树类  m_列表树
	HTEMP  m_hTemplate_group
	HIMAGE m_hExpand
	HIMAGE m_hExpandNo
	HIMAGE m_hVip
	HIMAGE m_hQZone
	HIMAGE m_hAvatarLarge
	HIMAGE m_hAvatarSmall
	
	def HWINDOW Init()
		BOOL bRes=炫彩_加载资源文件("resource.res")
		句柄=(HWINDOW)炫彩_加载布局文件("main.xml",0)
		
		m_列表树.句柄=(HELE)炫彩_取对象从名称("tree")
		m_列表树.启用连接线(FALSE,FALSE)
		m_列表树.显示水平滚动条(FALSE)
		m_列表树.置缩进(0)
		m_列表树.置项默认高度(28,54)
		
		m_列表树.置选择项模板文件("xml-template\\Tree_Item_friend.xml")
		m_列表树.置选择项模板文件("xml-template\\Tree_Item_friend_sel.xml")
		m_hTemplate_group=XTemp_Load(listItemTemp_type_tree,"xml-template\\Tree_Item_group.xml")
		
		m_hVip=XImage_LoadFile("image\\SuperVIP_LIGHT.png",FALSE)
		m_hQZone=XImage_LoadFile("image\\QQZone.png",FALSE)
		m_hAvatarSmall=XImage_LoadFile("image\\avatar_small.png",FALSE)
		m_hAvatarLarge=XImage_LoadFile("image\\avatar_large.png",FALSE)
		
		m_hExpand=图片_加载从文件("image\\expand.png",FALSE)
		m_hExpandNo=图片_加载从文件("image\\expandno.png",FALSE)
		图片_启用自动销毁(m_hExpandNo,FALSE)
		图片_启用自动销毁(m_hExpand,FALSE)
		
		数据适配器树类  适配器
		适配器.创建()
		适配器.添加列("name1")
		适配器.添加列("name2")
		适配器.添加列("name3")
		适配器.添加列("name4")
		适配器.添加列("name5")
		适配器.添加列("name6")
		m_列表树.绑定数据适配器(适配器.句柄)
		
		整型 nGroupID=0
		整型 nItemID=0
		计次循环 整型 iGroup=0 ;3
			文本 text = mkStr("好友分组 - ", iGroup)
			nGroupID=m_列表树.插入项文本(text,XC_ID_ROOT,XC_ID_LAST)
			m_列表树.置项高度(nGroupID,26,26)
			计次循环  整型 i=0; 5
				text = mkStr("我的好友-", i)
				nItemID=m_列表树.插入项文本(text,nGroupID,XC_ID_LAST)
				m_列表树.置项文本扩展(nItemID,"name2","我的个性签签名")
				m_列表树.置项图片扩展(nItemID,"name5",m_hVip)
				m_列表树.置项图片扩展(nItemID,"name6",m_hQZone)
				m_列表树.置项图片扩展(nItemID,"name3",m_hAvatarSmall)
				m_列表树.置项图片扩展(nItemID,"name4",m_hAvatarLarge)
		
		元素_注册事件CPP(m_列表树.句柄, XE_TREE_TEMP_CREATE, &CQQ::OnTemplateCreate) 
		元素_注册事件CPP(m_列表树.句柄, XE_TREE_TEMP_CREATE_END, &CQQ::OnTreeTemplateCreateEnd)
		元素_注册事件CPP(m_列表树.句柄, XE_DESTROY, &CQQ::OnDestroy)

		调整布局()
		显示(SW_SHOW)
		返回  句柄

	def 整型 OnTemplateCreate(tree_item_i* pItem, BOOL* pbHandled)
		调试输出("OnTemplateCreate()")
		如果 XC_ID_ERROR!=m_列表树.取第一个子项(pItem->nID)
			如果 m_hTemplate_group
				调试输出("m_hTemplate_group:", (int)m_hTemplate_group)
				pItem->hTemp= m_hTemplate_group
		调试输出("=====测试修改值====pbHandled===>>100")
		*pbHandled=100
		返回 0
	
	def 整型 OnTreeTemplateCreateEnd(tree_item_i* pItem, BOOL* pbHandled)
		调试输出("OnTreeTemplateCreateEnd():", pItem->nID)
		HELE hButtonExpand= (HELE)m_列表树.取模板对象(pItem->nID, 1)
		如果 hButtonExpand && 炫彩_判断元素(hButtonExpand)
			如果 m_hExpandNo
				按钮_添加背景图片(hButtonExpand,button_state_leave, m_hExpandNo)
				按钮_添加背景图片(hButtonExpand,button_state_stay, m_hExpandNo)
				按钮_添加背景图片(hButtonExpand,button_state_down, m_hExpandNo)
			
			如果 m_hExpand
				按钮_添加背景图片(hButtonExpand,button_state_check, m_hExpand)
			
			元素_启用背景透明(hButtonExpand, TRUE)
			按钮_置样式(hButtonExpand,button_style_default)
			元素_启用焦点(hButtonExpand, FALSE)
		返回 0

	def 整型 OnDestroy(BOOL* pbHandled)
		调试输出("OnDestroy()")
		如果 m_hTemplate_group; 模板_销毁(m_hTemplate_group)
		如果 m_hExpand; 图片_销毁(m_hExpand)
		如果 m_hExpandNo; 图片_销毁(m_hExpandNo)
		如果 m_hVip; 图片_销毁(m_hVip)
		如果 m_hQZone; 图片_销毁(m_hQZone)
		如果 m_hAvatarSmall; 图片_销毁(m_hAvatarSmall)
		如果 m_hAvatarLarge; 图片_销毁(m_hAvatarLarge)
		返回 0

def 整型 入口函数_窗口()   //窗口程序入口
	炫彩_初始化()       //初始化界面库
	CQQ  qq
	qq.Init()
	炫彩_运行()
	炫彩_退出()
	返回 0
