窗口类  主窗口
HELE    hLayoutContent=0  //布局内容
HELE    hCurPanel=0    //当前面板

def 无类型 SwitchTab(文本 strTy)
	如果 hCurPanel
		元素_销毁(hCurPanel)
	如果 "登陆"==strTy
		hCurPanel = (HELE)炫彩_加载布局文件("panel-login.xml",hLayoutContent)
		元素_置文本颜色((HELE)炫彩_取对象从名称("登陆"),0xFFFFFF,255)
		元素_置文本颜色((HELE)炫彩_取对象从名称("注册"),0x000000,255)
	否则
		hCurPanel = (HELE)炫彩_加载布局文件("panel-reg.xml",hLayoutContent)
		元素_置文本颜色((HELE)炫彩_取对象从名称("登陆"),0x000000,255)
		元素_置文本颜色((HELE)炫彩_取对象从名称("注册"),0xFFFFFF,255)
	主窗口.调整布局()
	主窗口.重绘()

def  整型 回调 OnBtnCheck_login(整型 bCheck, 整型 *pbHandled)
	如果 bCheck
		调试输出("按钮-登陆")
		SwitchTab("登陆")
	返回 0

def  整型 回调 OnBtnCheck_register(整型 bCheck, 整型 *pbHandled)
	如果 bCheck
		调试输出("按钮-注册")
		SwitchTab("注册")
	返回 0

def 整型 入口函数_窗口()   //窗口程序入口
	炫彩_初始化()       //初始化界面库
	
	炫彩_加载资源文件("resource.res")
	主窗口.句柄 = (HWINDOW)炫彩_加载布局文件("main.xml",0)
	如果 主窗口.句柄
		hLayoutContent =(HELE)炫彩_取对象从名称("content")
		SwitchTab("登陆")
		元素_注册事件C((HELE)炫彩_取对象从名称("登陆"), XE_BUTTON_CHECK, OnBtnCheck_login)
		元素_注册事件C((HELE)炫彩_取对象从名称("注册"), XE_BUTTON_CHECK, OnBtnCheck_register)
		
		主窗口.调整布局() //调整布局
		主窗口.显示(SW_SHOW) //显示窗口
		炫彩_运行()
	炫彩_退出()
	返回 0
